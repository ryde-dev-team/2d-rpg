
import pygame


class Graphics:

    def __init__(self, width, height):
        self.width = width
        self.height = height

        pygame.init()
        self.screen = pygame.display.set_mode((width,height))
        self.clock = pygame.time.Clock()
        self.isAlive = True

    def show(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.isAlive = False
                pygame.quit()
                quit()

        pygame.display.flip()
        self.clock.tick(60)